const FIRST_BRAIN: &str = "brains/Dorylines.bbrain";
// const SECOND_BRAIN: &str = "brains/random_search.brain";
const SECOND_BRAIN: &str = "brains/killerants.brain";
const MAP: &str = "maps/simple.map";

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    value: f32,
    depth: usize,
    max_depth: usize,
    offset: egui::Vec2,
    size: f32,
    separator: f32,
    #[serde(skip)] // This how you opt-out of serialization of a field
    game: Game,
    #[serde(skip)]
    step_jump: bool,
    #[serde(skip)]
    tick: usize,
    #[serde(skip)]
    step_jump_number: usize,
    #[serde(skip)]
    pheromone_black: bool,
    show_pheromone: bool,
    offset_text_food: f32,
    draw: bool,
    #[serde(skip)]
    show_side_panel: bool,
    #[serde(skip)]
    counter: usize,
    #[serde(skip)]
    slow_down: usize,
}

impl Default for TemplateApp {
    fn default() -> Self {
        use std::path::PathBuf;
        let path1 = PathBuf::from(FIRST_BRAIN);
        let path2 = PathBuf::from(SECOND_BRAIN);
        let map = PathBuf::from(MAP);
        let brains = vec![path1, path2];
        // let brains = vec!["./examples/brain1.brain".to_path_buf()];
        let brains = mechants::filter_brains(&brains);
        let game = mechants::create_game(&map, &brains, None);
        let max_depth = game.map().depth;
        Self {
            value: 2.7,
            depth: 0,
            max_depth: max_depth,
            offset: egui::Vec2::new(320f32, 60f32),
            size: 21f32,
            game,
            separator: 3.5f32,
            step_jump: false,
            tick: 0,
            step_jump_number: 100,
            pheromone_black: false,
            show_pheromone: true,
            offset_text_food: 21f32,
            draw: true,
            show_side_panel: true,
            counter: 0,
            slow_down: 1,
        }
    }
}

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }
        Default::default()
    }
}

impl eframe::App for TemplateApp {
    /// Called by the frame work to save state before shutdown.
    // fn save(&mut self, storage: &mut dyn eframe::Storage) {
    //     eframe::set_value(storage, eframe::APP_KEY, self);
    // }

    /// Called each time the UI needs repainting, which may be many times per second.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
        // For inspiration and more examples, go to https://emilk.github.io/egui

        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:

            egui::menu::bar(ui, |ui| {
                let is_web = cfg!(target_arch = "wasm32");
                if !is_web {
                    ui.menu_button("File", |ui| {
                        if ui.button("Quit").clicked() {
                            ctx.send_viewport_cmd(egui::ViewportCommand::Close);
                        }
                    });
                    ui.add_space(16.0);
                }

                egui::widgets::global_dark_light_mode_buttons(ui);
            });
        });
        if self.show_side_panel {
            egui::SidePanel::left("Test").show(ctx, |ui| {
                ui.heading("Test");
                ui.add(egui::Slider::new(&mut self.value, 0.0..=10.0).text("value"));
                if ui.button("Increment").clicked() {
                    self.value += 1.0;
                }
                ui.separator();
                ui.add(egui::Slider::new(&mut self.offset.x, 0.0..=1000.0).text("offset x"));
                ui.add(egui::Slider::new(&mut self.offset.y, 0.0..=1000.0).text("offset y"));
                ui.add(egui::Slider::new(&mut self.size, 0.0..=150.0).text("size"));
                ui.add(egui::Slider::new(&mut self.depth, 0..=(self.max_depth - 1)).text("depth"));
                ui.add(egui::Slider::new(&mut self.separator, 0.0..=20.0).text("separator"));
                ui.heading(format!("Tick : {}", self.tick));
                ui.checkbox(&mut self.step_jump, "Jump some steps");
                ui.add(egui::Slider::new(&mut self.step_jump_number, 1..=1000).text("number jump"));
                ui.add(
                    egui::Slider::new(&mut self.offset_text_food, 0.0..=100.0)
                        .text("offset text food"),
                );
                ui.add(egui::Slider::new(&mut self.slow_down, 1..=300).text("slow down factor"));
                ui.checkbox(&mut self.pheromone_black, "Feromone black");
                ui.checkbox(&mut self.show_pheromone, "Show pheromone");

                ui.checkbox(&mut self.draw, "Draw");

                ui.separator();

                // ui.heading("Brains");
                ui.label(format!("First brain {} in RED", FIRST_BRAIN));
                ui.label(format!("Second brain {} in GREEN", SECOND_BRAIN));

                ui.separator();

                ui.label(format!(
                    "Score team 0 : {}",
                    self.game.teams()[0].score.lock().unwrap()
                ));
                ui.label(format!(
                    "Score team 1 : {}",
                    self.game.teams()[1].score.lock().unwrap()
                ));
            });
        }

        egui::Window::new("Window").show(ctx, |ui| {
            ui.checkbox(&mut self.show_side_panel, "Show side panel");
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            // The central panel the region left after adding TopPanel's and SidePanel's
            // ui.heading("eframe template");
            self.counter += 1;
            if self.counter % self.slow_down == 0 {
                if self.step_jump {
                    for _ in 0..self.step_jump_number {
                        self.game.update();
                        self.tick += 1;
                    }
                } else {
                    self.game.update();
                    self.tick += 1;
                }
            }
            if self.draw {
                display(
                    &self.game,
                    self.depth,
                    self.size,
                    ui,
                    self.offset.clone(),
                    self.separator,
                    self.offset_text_food,
                    self.pheromone_black,
                    self.show_pheromone,
                );
            }
        });

        // To force update every frame
        ctx.request_repaint();
    }
}

use egui::{FontFamily, FontId, Pos2, Vec2};
use mechants::{game::Game, tile::Tile};

fn display(
    game: &Game,
    depth: usize,
    size: f32,
    ui: &mut egui::Ui,
    offset: egui::Vec2,
    separator: f32,
    offset_text_food: f32,
    pheronome_black: bool,
    show_pheromone: bool,
) {
    let map = game.map();
    let width_map = map.width;
    let height_map = map.height;
    let height = 2f32 * size;
    let width = 3f32.sqrt() * size;
    for i in (width_map * height_map * depth)..(width_map * height_map * (depth + 1)) {
        let j = i - (width_map * height_map * depth);
        let tile = &map.tiles[i];
        let pos_x = j % width_map;
        let pos_y = j / width_map;
        let separt = Vec2::new(separator * pos_x as f32, separator * pos_y as f32);

        let mut offset_line: f32 = 0.0;
        if pos_y % 2 == 0 {
            offset_line -= width / 2f32;
        } else {
            offset_line = separator / 2f32;
        }
        let pos = Pos2::new(
            pos_x as f32 * width + offset_line,
            pos_y as f32 * height * 0.75,
        );
        let hexa_high = (pos + offset) + separt;
        let points = hexagon(hexa_high, size);

        let (color_fill, color_stroke, ant, food, hole, pheromones) = match tile {
            Tile::Rock => (
                egui::Color32::BLACK,
                egui::Color32::BLACK,
                None,
                None,
                None,
                None,
            ),
            Tile::Home {
                team: 0,
                ant,
                food,
                pheromones,
                surface_hole_above,
                surface_hole_below,
                ..
            } => (
                egui::Color32::TRANSPARENT,
                egui::Color32::RED,
                ant.clone(),
                Some(food),
                Some(*surface_hole_above || *surface_hole_below),
                Some(pheromones),
            ),
            Tile::Home {
                food,
                surface_hole_above,
                surface_hole_below,
                pheromones,
                ant,
                ..
            } => (
                egui::Color32::TRANSPARENT,
                egui::Color32::GREEN,
                ant.clone(),
                Some(food),
                Some(*surface_hole_above || *surface_hole_below),
                Some(pheromones),
            ),
            Tile::Filled { pheromones, food } => (
                egui::Color32::DARK_GRAY,
                egui::Color32::BLACK,
                None,
                Some(food),
                None,
                Some(pheromones),
            ),
            Tile::Empty {
                food,
                surface_hole_above,
                surface_hole_below,
                pheromones,
                ant,
            } => (
                egui::Color32::from_rgb(200, 200, 200),
                egui::Color32::BLACK,
                ant.clone(),
                Some(food),
                Some(*surface_hole_above || *surface_hole_below),
                Some(pheromones),
            ),
        };
        ui.painter().add(egui::Shape::convex_polygon(
            points.clone(),
            color_fill,
            egui::Stroke::new(2f32, color_stroke),
        ));
        if let Some(hole) = hole {
            if hole {
                ui.painter().add(egui::Shape::circle_filled(
                    hexa_high + Vec2::new(0f32, height / 2f32),
                    size * 0.8 / 2f32,
                    egui::Color32::BLACK,
                ));
            }
        }

        if let Some(pheromones) = pheromones {
            if show_pheromone {
                let bit0_0 = (pheromones[0] & 0b0000_0001) != 0;
                let bit1_0 = (pheromones[0] & 0b0000_0010) != 0;
                let bit2_0 = (pheromones[0] & 0b0000_0100) != 0;
                let bit3_0 = (pheromones[0] & 0b0000_1000) != 0;
                let bit4_0 = (pheromones[0] & 0b0001_0000) != 0;
                let bit5_0 = (pheromones[0] & 0b0010_0000) != 0;
                let bit6_0 = (pheromones[0] & 0b0100_0000) != 0;
                let bit7_0 = (pheromones[0] & 0b1000_0000) != 0;

                let bit0_1 = (pheromones[1] & 0b0000_0001) != 0;
                let bit1_1 = (pheromones[1] & 0b0000_0010) != 0;
                let bit2_1 = (pheromones[1] & 0b0000_0100) != 0;
                let bit3_1 = (pheromones[1] & 0b0000_1000) != 0;
                let bit4_1 = (pheromones[1] & 0b0001_0000) != 0;
                let bit5_1 = (pheromones[1] & 0b0010_0000) != 0;
                let bit6_1 = (pheromones[1] & 0b0100_0000) != 0;
                let bit7_1 = (pheromones[1] & 0b1000_0000) != 0;

                let color_0 = egui::Color32::RED;
                let color_1 = egui::Color32::GREEN;
                // let size_horizon = Vec2::new(width / 4f32, height / 4f32);
                // let size_vert = Vec2::new(width / 4f32, height / 4f32);
                let radius = width / 10f32;
                let decal_horizon = Vec2::new(radius * 2f32, 0f32);

                // 1/8 = 2/16< 3/16 <4/16= 2/8
                let vec = vec![
                    hexa_high + Vec2::new(-3f32 * width / 8f32, 2f32 * height / 6f32),
                    hexa_high + Vec2::new(-3f32 * width / 8f32, 3f32 * height / 6f32),
                    hexa_high + Vec2::new(-3f32 * width / 8f32, 4f32 * height / 6f32),
                    hexa_high + Vec2::new(3f32 * width / 16f32, 2f32 * height / 6f32),
                    hexa_high + Vec2::new(3f32 * width / 16f32, 3f32 * height / 6f32),
                    hexa_high + Vec2::new(3f32 * width / 16f32, 4f32 * height / 6f32),
                    hexa_high + Vec2::new(-1f32 * width / 8f32, height / 6f32),
                    hexa_high + Vec2::new(-1f32 * width / 8f32, 5f32 * height / 6f32),
                ];
                if bit0_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[0], radius, color_0));
                }
                if bit0_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[0] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit1_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[1], radius, color_0));
                }
                if bit1_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[1] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit2_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[2], radius, color_0));
                }
                if bit2_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[2] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit3_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[3], radius, color_0));
                }
                if bit3_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[3] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit4_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[4], radius, color_0));
                }
                if bit4_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[4] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit5_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[5], radius, color_0));
                }
                if bit5_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[5] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit6_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[6], radius, color_0));
                }
                if bit6_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[6] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }

                if bit7_0 {
                    ui.painter()
                        .add(egui::Shape::circle_filled(vec[7], radius, color_0));
                }
                if bit7_1 {
                    ui.painter().add(egui::Shape::circle_filled(
                        vec[7] + decal_horizon,
                        radius,
                        color_1,
                    ));
                }
            }
        }

        if let Some(ant) = ant {
            if ant.position.level == depth {
                let color_stroke_fourmis = match ant {
                    mechants::ant::Ant { team: 0, .. } => egui::Color32::from_rgb(255, 0, 0),
                    mechants::ant::Ant { team: 1, .. } => egui::Color32::from_rgb(0, 255, 0),
                    _ => egui::Color32::from_rgb(0, 0, 255),
                };
                let color_fill_fourmis = match ant {
                    mechants::ant::Ant {
                        status: mechants::ant::Status::Stunned(_),
                        ..
                    }
                    | mechants::ant::Ant {
                        status: mechants::ant::Status::Stunning(_, _),
                        ..
                    } => egui::Color32::YELLOW,
                    mechants::ant::Ant { team: 0, .. } => egui::Color32::from_rgb(255, 0, 0),
                    mechants::ant::Ant { team: 1, .. } => egui::Color32::from_rgb(0, 255, 0),
                    _ => egui::Color32::from_rgb(0, 0, 255),
                };

                let points = ants(hexa_high, size, ant.direction);
                ui.painter().add(egui::Shape::convex_polygon(
                    points,
                    color_fill_fourmis,
                    egui::Stroke::new(1f32, color_stroke_fourmis),
                ));

                if ant.storage == mechants::ant::Storage::Food {
                    ui.painter().add(egui::Shape::circle_filled(
                        hexa_high + Vec2::new(0f32, height / 2f32),
                        size * 0.3,
                        egui::Color32::from_rgb(255, 99, 71),
                    ));
                }
            }
        }

        if let Some(food) = food {
            if *food != 0 {
                // Draw text number food
                let text = format!("{}", food);
                ui.painter().text(
                    hexa_high + Vec2::new(0f32, offset_text_food),
                    egui::Align2::CENTER_CENTER,
                    text,
                    FontId::new(size, FontFamily::Proportional),
                    egui::Color32::from_rgb(255, 99, 71),
                );
            }
        }

        if let Some(pheromones) = pheromones {
            if pheronome_black {
                if pheromones[0] > 0 || pheromones[1] > 0 {
                    ui.painter().add(egui::Shape::convex_polygon(
                        points,
                        egui::Color32::BLACK,
                        egui::Stroke::new(0f32, color_stroke),
                    ));
                } else {
                    ui.painter().add(egui::Shape::convex_polygon(
                        points,
                        egui::Color32::WHITE,
                        egui::Stroke::new(0f32, color_stroke),
                    ));
                }
            }
        }
    }
}

fn hexagon(pos: Pos2, size: f32) -> Vec<egui::Pos2> {
    let x = pos.x;
    let y = pos.y;
    let mut points = Vec::new();

    let height = 2f32 * size;
    let width = 3f32.sqrt() * size;

    points.push(egui::Pos2::new(x, y));
    points.push(egui::Pos2::new(x + (width / 2f32), y + (height / 4f32)));
    points.push(egui::Pos2::new(
        x + (width / 2f32),
        y + (3f32 * height / 4f32),
    ));
    points.push(egui::Pos2::new(x, y + height));
    points.push(egui::Pos2::new(
        x - (width / 2f32),
        y + (3f32 * height / 4f32),
    ));
    points.push(egui::Pos2::new(x - width / 2f32, y + (height / 4f32)));

    points
}

fn ants(pos: Pos2, size: f32, direction: mechants::map::Direction) -> Vec<egui::Pos2> {
    let x = pos.x;
    let y = pos.y;
    let mut points = Vec::new();

    let height = 2f32 * size;
    let width = 3f32.sqrt() * size;

    match direction {
        mechants::map::Direction::East => {
            points.push(egui::Pos2::new(x + (width / 2f32), y + (height / 2f32)));
            points.push(egui::Pos2::new(
                x - (width / 2f32),
                y + (height * 3f32 / 4f32),
            ));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(x - (width / 2f32), y + (height / 4f32)));
        }
        mechants::map::Direction::NorthEast => {
            points.push(egui::Pos2::new(x + (width / 4f32), y + (height / 8f32)));
            points.push(egui::Pos2::new(x, y + height));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(
                x - (width / 2f32),
                y + (height * 3f32 / 4f32),
            ));
        }
        mechants::map::Direction::SouthEast => {
            points.push(egui::Pos2::new(
                x + (width / 4f32),
                y + (height * 7f32 / 8f32),
            ));
            points.push(egui::Pos2::new(x - (width / 2f32), y + (height / 4f32)));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(x, y));
        }
        mechants::map::Direction::SouthWest => {
            points.push(egui::Pos2::new(
                x - (width / 4f32),
                y + (height * 7f32 / 8f32),
            ));
            points.push(egui::Pos2::new(x, y));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(x + (width / 2f32), y + (height / 4f32)));
        }
        mechants::map::Direction::West => {
            points.push(egui::Pos2::new(x - (width / 2f32), y + (height / 2f32)));
            points.push(egui::Pos2::new(x + (width / 2f32), y + (height / 4f32)));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(
                x + (width / 2f32),
                y + (height * 3f32 / 4f32),
            ));
        }
        mechants::map::Direction::NorthWest => {
            points.push(egui::Pos2::new(x - (width / 4f32), y + (height / 8f32)));
            points.push(egui::Pos2::new(
                x + (width / 2f32),
                y + (height * 3f32 / 4f32),
            ));
            points.push(egui::Pos2::new(x, y + (height / 2f32)));
            points.push(egui::Pos2::new(x, y + height));
        }
        mechants::map::Direction::Above => todo!("unreachable"),
        mechants::map::Direction::Below => todo!("unreachable"),
    }

    points
}
